package com.example.leixin.helloworld.advanceskill;

import android.os.Parcel;
import android.os.Parcelable;

public class Human implements Parcelable {

    private String name;
    private int age;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(age);
    }

    public static final Parcelable.Creator<Human> CREATOR = new Parcelable.Creator<Human>() {

        @Override
        public Human createFromParcel(Parcel source) {
            Human human = new Human();
            human.name = source.readString();
            human.age = source.readInt();
            return human;
        }

        @Override
        public Human[] newArray(int size) {
            return new Human[size];
        }
    };

    public static void main(String[] args) {
//        Intent intent = getIntent();
//        Human human = (Human)intent.getParcelableExtra("human_data");
    }
}
