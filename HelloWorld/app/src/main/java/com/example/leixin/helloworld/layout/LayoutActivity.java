package com.example.leixin.helloworld.layout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.MainActivity;
import com.example.leixin.helloworld.R;

public class LayoutActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout);

        Button button_linear_layout = (Button) findViewById(R.id.button_linear_layout);
        Button button_relative_layout = (Button) findViewById(R.id.button_relative_layout);
        Button button_frame_layout = (Button) findViewById(R.id.button_frame_layout);
        Button button_percent_layout = (Button) findViewById(R.id.button_percent_layout);
        Button button_custom_title_layout = (Button) findViewById(R.id.button_custom_title_layout);
        Button button_custom_control_layout = (Button) findViewById(R.id.button_custom_control_layout);
        button_linear_layout.setOnClickListener(this);
        button_relative_layout.setOnClickListener(this);
        button_frame_layout.setOnClickListener(this);
        button_percent_layout.setOnClickListener(this);
        button_custom_title_layout.setOnClickListener(this);
        button_custom_control_layout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_linear_layout:
                Intent intent = new Intent(LayoutActivity.this, LinearLayoutActivity.class);
                startActivity(intent);
                break;
            case R.id.button_relative_layout:
                Intent intent2 = new Intent(LayoutActivity.this, RelativeLayoutActivity.class);
                startActivity(intent2);
                break;
            case R.id.button_frame_layout:
                Intent intent3 = new Intent(LayoutActivity.this, FrameLayoutActivity.class);
                startActivity(intent3);
                break;
            case R.id.button_percent_layout:
                Intent intent4 = new Intent(LayoutActivity.this, PercentFrameLayoutActivity.class);
                startActivity(intent4);
                break;
            case R.id.button_custom_title_layout:
                Intent intent5 = new Intent(LayoutActivity.this, CustomTitleActivity.class);
                startActivity(intent5);
                break;
            case R.id.button_custom_control_layout:
                Intent intent6 = new Intent(LayoutActivity.this, CustomControlActivity.class);
                startActivity(intent6);
                break;

            default:
                break;
        }
    }
}
