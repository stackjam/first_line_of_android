package com.example.leixin.helloworld.launchmode;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.leixin.helloworld.R;

public class CalledForResultActivity extends BaseActivity {

    private static final String TAG = "CalledForResultActivity";

    String data = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_for_result_layout);

        Intent intent = getIntent();
        data = intent.getStringExtra("extra_data");

        Log.d(TAG, "onCreate: Task id is " + getTaskId());

        Button button2 = (Button) findViewById(R.id.button_back_to_launch);
        button2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                prepareGift();
                finish();
            }
        });

        Button button_call_a_standard = (Button) findViewById(R.id.button_call_a_standard);
        button_call_a_standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalledForResultActivity.this, StandardActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(CalledForResultActivity.this,"传来的数据：" + data, Toast.LENGTH_SHORT).show();
    }

    private void prepareGift() {
        Intent intent = new Intent();
        intent.putExtra("data_return", "Hello LaunchActivity");
        setResult(RESULT_OK, intent);
    }

    @Override
    public void onBackPressed() {
        prepareGift();
//        调用finish或者super的onBackPressed方法回到上一个activity
//        finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }
}
