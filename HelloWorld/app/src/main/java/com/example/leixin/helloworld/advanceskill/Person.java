package com.example.leixin.helloworld.advanceskill;

import android.content.Intent;

import com.example.leixin.helloworld.MainActivity;

import java.io.Serializable;

public class Person implements Serializable {

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) {
        //send sample
        Person person = new Person();
        person.setName("Tom");
        person.setAge(28);
        Intent intent = new Intent(MyApplication.getContext(), MainActivity.class);
        intent.putExtra("person_data", person);
        MyApplication.getContext().startActivity(intent);

        //receive sample
//        Intent it = getIntent();
//        Person p = (Person)it.getSerializableExtra("person_data");
    }

}
