package com.example.leixin.helloworld.persist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;

public class PersistActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persist);

        Button button_file_persist = (Button) findViewById(R.id.button_file_persist);
        button_file_persist.setOnClickListener(this);
        Button button_shared_preference = (Button) findViewById(R.id.button_shared_preference);
        button_shared_preference.setOnClickListener(this);
        Button button_remember_me = (Button) findViewById(R.id.button_remember_me);
        button_remember_me.setOnClickListener(this);
        Button button_sqllite = (Button) findViewById(R.id.button_sqllite);
        button_sqllite.setOnClickListener(this);
        Button button_litepal = (Button) findViewById(R.id.button_litepal);
        button_litepal.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_file_persist:
                Intent intent = new Intent(PersistActivity.this, FilePersistActivity.class);
                startActivity(intent);
                break;
            case R.id.button_shared_preference:
                Intent intent2 = new Intent(PersistActivity.this, SharedPreferenceActivity.class);
                startActivity(intent2);
                break;
            case R.id.button_remember_me:
                Intent intent3 = new Intent(PersistActivity.this, RememberMeActivity.class);
                startActivity(intent3);
                break;
            case R.id.button_sqllite:
                Intent intent4 = new Intent(PersistActivity.this, SqliteActivity.class);
                startActivity(intent4);
                break;
            case R.id.button_litepal:
                Intent intent5 = new Intent(PersistActivity.this, LitePalActivity.class);
                startActivity(intent5);
                break;
        }
    }
}
