package com.example.leixin.helloworld.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class BootCompleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Mate10上此自receiver无效
        Toast.makeText(context, "Boot Complete", Toast.LENGTH_LONG).show();
    }
}
