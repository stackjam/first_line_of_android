package com.example.leixin.helloworld;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.leixin.helloworld.broadcast.DynamicBroadcastActivity;
import com.example.leixin.helloworld.contentprovider.ContentProviderActivity;
import com.example.leixin.helloworld.fragment.FragmentSamplesActivity;
import com.example.leixin.helloworld.launchmode.LaunchActivity;
import com.example.leixin.helloworld.layout.LayoutActivity;
import com.example.leixin.helloworld.lbs.LBSActivity;
import com.example.leixin.helloworld.lifecycle.LifeCycleActivity;
import com.example.leixin.helloworld.list.ListActivity;
import com.example.leixin.helloworld.materialdesign.MaterialDesignActivity;
import com.example.leixin.helloworld.net.NetActivity;
import com.example.leixin.helloworld.notification.NotificationActivity;
import com.example.leixin.helloworld.persist.PersistActivity;
import com.example.leixin.helloworld.service.ServiceActivity;
import com.example.leixin.helloworld.weather.WeatherAcitvity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: Hello first debug message!");

        Button button_life_cycle = (Button) findViewById(R.id.button_life_cycle);
        button_life_cycle.setOnClickListener(this);
        Button button_layout = (Button) findViewById(R.id.button_layout);
        button_layout.setOnClickListener(this);
        Button button_ui = (Button) findViewById(R.id.button_ui);
        button_ui.setOnClickListener(this);
        Button button_launchmode = (Button) findViewById(R.id.button_launchmode);
        button_launchmode.setOnClickListener(this);
        Button button_list = (Button) findViewById(R.id.button_list);
        button_list.setOnClickListener(this);
        Button button_fragment = (Button) findViewById(R.id.button_fragment);
        button_fragment.setOnClickListener(this);
        Button button_broadcast = (Button) findViewById(R.id.button_broadcast);
        button_broadcast.setOnClickListener(this);
        Button button_persist = (Button) findViewById(R.id.button_persist);
        button_persist.setOnClickListener(this);
        Button button_content_provider = (Button) findViewById(R.id.button_content_provider);
        button_content_provider.setOnClickListener(this);
        Button button_notification = (Button) findViewById(R.id.button_notification);
        button_notification.setOnClickListener(this);
        Button button_net = (Button) findViewById(R.id.button_net);
        button_net.setOnClickListener(this);
        Button button_service = (Button) findViewById(R.id.button_service);
        button_service.setOnClickListener(this);
        Button button_lbs = (Button) findViewById(R.id.button_lbs);
        button_lbs.setOnClickListener(this);
        Button button_materialdesign = (Button) findViewById(R.id.button_materialdesign);
        button_materialdesign.setOnClickListener(this);
        Button button_weather = (Button) findViewById(R.id.button_weather);
        button_weather.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_life_cycle:
                Intent intent3 = new Intent(MainActivity.this, LifeCycleActivity.class);
                startActivity(intent3);
                break;
            case R.id.button_layout:
                Intent intent1 = new Intent(MainActivity.this, LayoutActivity.class);
                startActivity(intent1);
                break;
            case R.id.button_ui:
                Intent intent2 = new Intent(MainActivity.this, UIActivity.class);
                startActivity(intent2);
                break;
            case R.id.button_launchmode:
                Intent intent4 = new Intent(MainActivity.this, LaunchActivity.class);
                startActivity(intent4);
                break;
            case R.id.button_list:
                Intent intent5 = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intent5);
                break;
            case R.id.button_fragment:
                Intent intent6 = new Intent(MainActivity.this, FragmentSamplesActivity.class);
                startActivity(intent6);
                break;
            case R.id.button_broadcast:
                Intent intent7 = new Intent(MainActivity.this, DynamicBroadcastActivity.class);
                startActivity(intent7);
                break;
            case R.id.button_persist:
                Intent intent8 = new Intent(MainActivity.this, PersistActivity.class);
                startActivity(intent8);
                break;
            case R.id.button_content_provider:
                Intent intent9 = new Intent(MainActivity.this, ContentProviderActivity.class);
                startActivity(intent9);
                break;
            case R.id.button_notification:
                Intent intent10 = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(intent10);
                break;
            case R.id.button_net:
                Intent intent11 = new Intent(MainActivity.this, NetActivity.class);
                startActivity(intent11);
                break;
            case R.id.button_service:
                Intent intent12 = new Intent(MainActivity.this, ServiceActivity.class);
                startActivity(intent12);
                break;
            case R.id.button_lbs:
                Intent intent13 = new Intent(MainActivity.this, LBSActivity.class);
                startActivity(intent13);
                break;
            case R.id.button_materialdesign:
                Intent intent14 = new Intent(MainActivity.this, MaterialDesignActivity.class);
                startActivity(intent14);
                break;
            case R.id.button_weather:
                Intent intent15 = new Intent(MainActivity.this, WeatherAcitvity.class);
                startActivity(intent15);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_item:
                Toast.makeText(this, "You clicked Add", Toast.LENGTH_SHORT).show();
                break;
            case R.id.remove_item:
                Toast.makeText(this, "You clicked Remove", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

        return true;
    }
}
