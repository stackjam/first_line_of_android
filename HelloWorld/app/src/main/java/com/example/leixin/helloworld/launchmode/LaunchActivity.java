package com.example.leixin.helloworld.launchmode;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.leixin.helloworld.R;

public class LaunchActivity extends AppCompatActivity {

    private static final String TAG = "LaunchActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        // 显式调用并传参,且接受回传参数
        Button button_standard_explict_with_params = (Button) findViewById(R.id.button_standard_explict_with_params);
        button_standard_explict_with_params.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String data = "Hello CalledForResultActivity";
                Intent intent = new Intent(LaunchActivity.this, CalledForResultActivity.class);
                intent.putExtra("extra_data", data);
                // startActivityForResult似乎会干扰到SingleInstance模式
                startActivityForResult(intent, 1);
            }
        });

        // 隐式调用
        Button button_singleInstance_implict = (Button) findViewById(R.id.button_singleInstance_implict);
        button_singleInstance_implict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("com.example.activitytest.ACTION_START");
                intent.addCategory("com.example.activitytest.MY_CATAGORY");
                startActivity(intent);
            }
        });

        // 调用标准模式的
        Button button_standard = (Button) findViewById(R.id.button_standard);
        button_standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LaunchActivity.this, StandardActivity.class);
                startActivity(intent);
            }
        });

        // 调用singleTop模式的
        Button button_singleTop = (Button) findViewById(R.id.button_singleTop);
        button_singleTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LaunchActivity.this, SingleTopActivity.class);
                startActivity(intent);
            }
        });

        // 调用SingleInstance模式的
        Button button_singleInstance = (Button) findViewById(R.id.button_singleInstance);
        button_singleInstance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LaunchActivity.this, SingleInstanceActivity.class);
                startActivity(intent);
            }
        });

        Button button_call_browser = (Button) findViewById(R.id.button_call_browser);
        button_call_browser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://www.baidu.com"));
                startActivity(intent);
            }
        });

        Button button_dial = (Button) findViewById(R.id.button_dial);
        button_dial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:10086"));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    String returnedData = data.getStringExtra("data_return");
                    Toast.makeText(LaunchActivity.this, "返回的数据:" + returnedData, Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }
}
