package com.example.leixin.helloworld.launchmode;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.launchmode.utils.ActivityCollector;

import com.example.leixin.helloworld.R;

public class ImplictActivity extends BaseActivity {

    private static final String TAG = "ImplictActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_implicity);
        Log.d(TAG, "onCreate: Task id is " + getTaskId());

        Button finishAll = (Button)findViewById(R.id.button_3);
        finishAll.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ActivityCollector.finishAll();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
    }
}
