package com.example.leixin.helloworld.broadcast;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;
import com.example.leixin.helloworld.launchmode.BaseActivity;

public class LoginSuccActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_succ);

        Button button_force_offline = (Button) findViewById(R.id.button_force_offline);
        button_force_offline.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_force_offline:
                Intent intent = new Intent("com.example.leixin.helloworld.broadcast.FORCE_OFFLINE");
                sendBroadcast(intent);
                break;
        }
    }
}
