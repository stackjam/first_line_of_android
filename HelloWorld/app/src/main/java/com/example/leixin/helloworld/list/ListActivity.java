package com.example.leixin.helloworld.list;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;

public class ListActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Button button_listview = (Button) findViewById(R.id.button_listview);
        button_listview.setOnClickListener(this);

        Button button_listview_custom = (Button) findViewById(R.id.button_listview_custom);
        button_listview_custom.setOnClickListener(this);

        Button button_listview_custom_refined = (Button) findViewById(R.id.button_listview_custom_refined);
        button_listview_custom_refined.setOnClickListener(this);

        Button button_recycler_view = (Button) findViewById(R.id.button_recycler_view);
        button_recycler_view.setOnClickListener(this);

        Button button_recycler_view_horizontal = (Button) findViewById(R.id.button_recycler_view_horizontal);
        button_recycler_view_horizontal.setOnClickListener(this);

        Button button_recycler_view_staggered = (Button) findViewById(R.id.button_recycler_view_staggered);
        button_recycler_view_staggered.setOnClickListener(this);

        Button button_recycler_view_chat= (Button) findViewById(R.id.button_recycler_view_chat);
        button_recycler_view_chat.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_listview:
                Intent intent = new Intent(ListActivity.this, SimpleListActivity.class);
                startActivity(intent);
                break;
            case R.id.button_listview_custom:
                Intent intent2 = new Intent(ListActivity.this, CustomListActivity.class);
                startActivity(intent2);
                break;
            case R.id.button_listview_custom_refined:
                Intent intent3 = new Intent(ListActivity.this, RefinedCustomListActivity.class);
                startActivity(intent3);
                break;
            case R.id.button_recycler_view:
                Intent intent4 = new Intent(ListActivity.this, RecyclerviewActivity.class);
                startActivity(intent4);
                break;
            case R.id.button_recycler_view_horizontal:
                Intent intent5 = new Intent(ListActivity.this, RecyclerviewHorizontalActivity.class);
                startActivity(intent5);
                break;
            case R.id.button_recycler_view_staggered:
                Intent intent6 = new Intent(ListActivity.this, RecyclerviewStaggeredActivity.class);
                startActivity(intent6);
                break;
            case R.id.button_recycler_view_chat:
                Intent intent7 = new Intent(ListActivity.this, RecyclerviewMessageActivity.class);
                startActivity(intent7);
                break;
            default:
                break;
        }
    }
}
