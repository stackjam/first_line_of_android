package com.example.leixin.helloworld.net;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.leixin.helloworld.R;

public class WebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        WebView webView = (WebView) findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);// 默认不开启
        webView.setWebViewClient(new WebViewClient()); // 使用了本语句，将会在本WebView控件内载入页面，不使用则跳转到系统默认浏览器来载入页面
        webView.loadUrl("http://www.sina.com");
    }
}
