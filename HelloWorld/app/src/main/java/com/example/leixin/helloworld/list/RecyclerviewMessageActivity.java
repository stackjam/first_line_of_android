package com.example.leixin.helloworld.list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leixin.helloworld.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerviewMessageActivity extends AppCompatActivity {

    private List<Msg> mMsgList = new ArrayList<>();
    private EditText inputText;
    private RecyclerView recyclerView;
    private MsgAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerview_chat);

        initMsgs();

        inputText = (EditText) findViewById(R.id.input_text);
        Button send = (Button) findViewById(R.id.send);

        recyclerView = (RecyclerView) findViewById(R.id.msg_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MsgAdapter(mMsgList);
        recyclerView.setAdapter(adapter);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = inputText.getText().toString();
                if (!"".equals(content)) {
                    Msg msg = new Msg(content, Msg.Type_sent);
                    mMsgList.add(msg);
                    adapter.notifyItemInserted(mMsgList.size() - 1);
                    recyclerView.scrollToPosition(mMsgList.size() - 1);
                    inputText.setText("");
                }
            }
        });
    }

    private void initMsgs() {
        Msg msg1 = new Msg("Hello guy.", Msg.Type_receviced);
        mMsgList.add(msg1);
        Msg msg2 = new Msg("Hello. Who is that", Msg.Type_sent);
        mMsgList.add(msg2);
        Msg msg3 = new Msg("This is Tom. Nice talking to you.", Msg.Type_receviced);
        mMsgList.add(msg3);
    }

    class MsgAdapter extends RecyclerView.Adapter<MsgAdapter.ViewHolder> {

        class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout leftLayout;
            LinearLayout rightLayout;

            TextView leftMsg;
            TextView rightMsg;

            public ViewHolder(View view) {
                super(view);
                leftLayout = (LinearLayout) view.findViewById(R.id.left_layout);
                rightLayout = (LinearLayout) view.findViewById(R.id.right_layout);
                leftMsg = (TextView) view.findViewById(R.id.left_msg);
                rightMsg = (TextView) view.findViewById(R.id.right_msg);
            }
        }

        private List<Msg> mMsgList;

        public MsgAdapter(List<Msg> msgList) {
            mMsgList = msgList;
        }

        @Override
        public MsgAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.msg_item, parent, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(MsgAdapter.ViewHolder holder, int position) {
            Msg msg = mMsgList.get(position);
            if (msg.getType() == Msg.Type_receviced) {
                holder.leftLayout.setVisibility(View.VISIBLE);
                holder.rightLayout.setVisibility(View.GONE);
                holder.leftMsg.setText(msg.getContent());
            } else if (msg.getType() == Msg.Type_sent) {
                holder.leftLayout.setVisibility(View.GONE);
                holder.rightLayout.setVisibility(View.VISIBLE);
                holder.rightMsg.setText(msg.getContent());
            }
        }

        @Override
        public int getItemCount() {
            return mMsgList.size();
        }
    }
}
