package com.example.leixin.helloworld.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class ThirdCustomReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "receiver in ThirdCustomReceiver", Toast.LENGTH_SHORT).show();
        abortBroadcast();
    }
}
