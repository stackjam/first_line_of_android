package com.example.leixin.helloworld.persist;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;

public class SqliteActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "SqliteActivity";

    private MyDatabaseHelper myDatabaseHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite);

        myDatabaseHelper = new MyDatabaseHelper(this, "BookStore.db", null, 2);

        Button button_sqlite_create = (Button) findViewById(R.id.button_sqlite_create);
        button_sqlite_create.setOnClickListener(this);
        Button button_sqlite_add = (Button) findViewById(R.id.button_sqlite_add);
        button_sqlite_add.setOnClickListener(this);
        Button button_sqlite_update = (Button) findViewById(R.id.button_sqlite_update);
        button_sqlite_update.setOnClickListener(this);
        Button button_sqlite_delete = (Button) findViewById(R.id.button_sqlite_delete);
        button_sqlite_delete.setOnClickListener(this);
        Button button_sqlite_query = (Button) findViewById(R.id.button_sqlite_query);
        button_sqlite_query.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_sqlite_create:
                myDatabaseHelper.getWritableDatabase();
                break;
            case R.id.button_sqlite_add:
                SQLiteDatabase sqLiteDatabase = myDatabaseHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put("name", "The Da Vinci Code");
                values.put("author", "Dan Brown");
                values.put("pages", 454);
                values.put("price", 16.96);
                sqLiteDatabase.insert("Book", null, values);
                values.clear();

                values.put("name", "The Lost Symbol");
                values.put("author", "Dan Brown");
                values.put("pages", 510);
                values.put("price", 19.95);
                sqLiteDatabase.insert("Book", null, values);
                break;
            case R.id.button_sqlite_update:
                SQLiteDatabase sqLiteDatabase1 = myDatabaseHelper.getWritableDatabase();
                ContentValues values1 = new ContentValues();
                values1.put("price", 10.99);
                sqLiteDatabase1.update("Book", values1, "name = ?", new String[]{"The Da Vinci Code"});
                break;
            case R.id.button_sqlite_delete:
                SQLiteDatabase sqLiteDatabase2 = myDatabaseHelper.getWritableDatabase();
                sqLiteDatabase2.delete("Book", "pages > ?", new String[]{"500"});
                break;
            case R.id.button_sqlite_query:
                SQLiteDatabase sqLiteDatabase3 = myDatabaseHelper.getWritableDatabase();
                Cursor cursor = sqLiteDatabase3.query("Book", null, null, null, null, null, null);
                if (cursor.moveToFirst()) {
                    do {
                        String name = cursor.getString(cursor.getColumnIndex("name"));
                        String author = cursor.getString(cursor.getColumnIndex("author"));
                        int pages = cursor.getInt(cursor.getColumnIndex("pages"));
                        double price = cursor.getDouble(cursor.getColumnIndex("price"));
                        Log.d(TAG, "onClick: book name is " + name);
                        Log.d(TAG, "onClick: book author is " + author);
                        Log.d(TAG, "onClick: book pages is " + pages);
                        Log.d(TAG, "onClick: book price is " + price);
                    } while (cursor.moveToNext());
                }
                cursor.close();
                break;
        }
    }
}
