package com.example.leixin.helloworld.launchmode;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;

public class SingleTopActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_top);

        Button button_call_self_single_top = (Button) findViewById(R.id.button_call_self_single_top);
        button_call_self_single_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 显式调用
                Intent intent = new Intent(SingleTopActivity.this, SingleTopActivity.class);
                startActivity(intent);
            }
        });

        Button button_call_standard = (Button) findViewById(R.id.button_call_standard);
        button_call_standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 显式调用
                Intent intent = new Intent(SingleTopActivity.this, StandardActivity.class);
                startActivity(intent);
            }
        });

    }
}
