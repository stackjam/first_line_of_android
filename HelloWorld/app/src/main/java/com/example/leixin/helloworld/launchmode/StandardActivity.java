package com.example.leixin.helloworld.launchmode;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;
import com.example.leixin.helloworld.launchmode.utils.ActivityCollector;

public class StandardActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standard);

        Button button_call_self_standard = (Button) findViewById(R.id.button_call_self_standard);
        button_call_self_standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 显式调用
                Intent intent = new Intent(StandardActivity.this, StandardActivity.class);
                startActivity(intent);
            }
        });

        Button button_call_singleTask = (Button) findViewById(R.id.button_call_singleTask);
        button_call_singleTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 显式调用
                Intent intent = new Intent(StandardActivity.this, LaunchActivity.class);
                startActivity(intent);
            }
        });

        Button button_finish_all = (Button) findViewById(R.id.button_finish_all);
        button_finish_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 显式调用
                ActivityCollector.finishAll();
            }
        });
    }
}
