package com.example.leixin.helloworld.persist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.leixin.helloworld.R;
import com.example.leixin.helloworld.broadcast.LoginSuccActivity;
import com.example.leixin.helloworld.launchmode.BaseActivity;

public class RememberMeActivity extends BaseActivity implements View.OnClickListener {

    private EditText accountEdit;
    private EditText passwordEdit;
    private Button button_login;
    private CheckBox rememberPassword;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_remember);

        accountEdit = (EditText) findViewById(R.id.account);
        passwordEdit = (EditText) findViewById(R.id.password);
        button_login = (Button) findViewById(R.id.button_login);
        rememberPassword = (CheckBox) findViewById(R.id.remember_password);

        button_login.setOnClickListener(this);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isRemember = pref.getBoolean("remember_password", false);
        if (isRemember) {
            String account = pref.getString("account", "");
            String password = pref.getString("password", "");
            accountEdit.setText(account);
            passwordEdit.setText(password);
            rememberPassword.setChecked(true);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_login:
                String account = accountEdit.getText().toString();
                String password = passwordEdit.getText().toString();
                if (account.equals("admin") && password.equals("123456")) {
                    editor = pref.edit();
                    if (rememberPassword.isChecked()) {
                        editor.putBoolean("remember_password", true);
                        editor.putString("account", account);
                        editor.putString("password", password);
                    } else {
                        editor.clear();
                    }
                    editor.apply();

                    Intent intent = new Intent(RememberMeActivity.this, LoginSuccActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(RememberMeActivity.this, "account or password is invalid", Toast.LENGTH_SHORT).show();
                }
        }
    }
}
