package com.example.leixin.helloworld;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class UIActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editText;
    private ImageView imageView;
    private ProgressBar progressBar;
    private ProgressBar progressBarIncrease;
    boolean isImageOne = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui);

        Button button_send = (Button) findViewById(R.id.button_send);
        Button button_change_image = (Button) findViewById(R.id.button_change_image);
        Button button_control_progress = (Button) findViewById(R.id.button_control_progress);
        Button button_control_progress_increase = (Button) findViewById(R.id.button_control_progress_increase);
        Button button_dialog = (Button) findViewById(R.id.button_dialog);
        Button button_progress_dialog = (Button) findViewById(R.id.button_progress_dialog);

        editText = (EditText) findViewById(R.id.input_message);
        imageView = (ImageView) findViewById(R.id.image_view);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBarIncrease = (ProgressBar) findViewById(R.id.progress_bar_increase);
        button_send.setOnClickListener(this);
        button_change_image.setOnClickListener(this);
        button_control_progress.setOnClickListener(this);
        button_control_progress_increase.setOnClickListener(this);
        button_dialog.setOnClickListener(this);
        button_progress_dialog.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_send:
                String inputText = editText.getText().toString();
                Toast.makeText(UIActivity.this, inputText, Toast.LENGTH_SHORT).show();
                break;
            case R.id.button_change_image:
                if (isImageOne) {
                    imageView.setImageResource(R.drawable.img_2);
                } else {
                    imageView.setImageResource(R.drawable.img_1);
                }
                isImageOne = !isImageOne;
                break;
            case R.id.button_control_progress:
                if (progressBar.getVisibility() == View.GONE) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
                break;
            case R.id.button_control_progress_increase:
                int progress = progressBarIncrease.getProgress();
                progress = (progress + 10) % 100;
                progressBarIncrease.setProgress(progress);
                break;
            case R.id.button_dialog:
                AlertDialog.Builder dialog = new AlertDialog.Builder(UIActivity.this);
                dialog.setTitle("This is Dialog");
                dialog.setMessage("Something important!");
                dialog.setCancelable(false);
                dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(UIActivity.this, "you clicked OK", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(UIActivity.this, "you clicked Cancel", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.show();
                break;
            case R.id.button_progress_dialog:
                ProgressDialog progressDialog = new ProgressDialog(UIActivity.this);
                progressDialog.setTitle("This is ProgressDialog");
                progressDialog.setMessage("Loading...");
                progressDialog.setCancelable(true);
                progressDialog.show();
                break;
            default:
                break;
        }
    }
}
