package com.example.leixin.helloworld.weather;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.leixin.helloworld.R;

public class WeatherAcitvity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPreferences.getString("weather", null) != null) {
            Intent intent = new Intent(this, WeatherDetailAcitvity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        ChooseAreaFragment chooseAreaFragment = (ChooseAreaFragment) getSupportFragmentManager().findFragmentById(R.id.choose_area_fragment);
        if (chooseAreaFragment.isBackable()) {
            chooseAreaFragment.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
