package com.example.leixin.helloworld.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.leixin.helloworld.R;

public class IntelligentFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_intelligent);
    }
}
