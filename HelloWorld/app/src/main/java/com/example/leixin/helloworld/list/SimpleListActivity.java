package com.example.leixin.helloworld.list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.leixin.helloworld.R;

import java.util.ArrayList;
import java.util.List;

public class SimpleListActivity extends AppCompatActivity {

    private List<String> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list);

        initFruits();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SimpleListActivity.this,
                android.R.layout.simple_list_item_1, data);
        ListView listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(adapter);
    }

    private void initFruits() {
        for (int i = 0; i < 10; i++) {
            data.add("Apple");
            data.add("Banana");
            data.add("Orange");
            data.add("Watermelon");
            data.add("Pear");
            data.add("Grape");
            data.add("Pineapple");
            data.add("Strawberry");
            data.add("Cherry");
            data.add("Kiwi");
        }
    }
}
