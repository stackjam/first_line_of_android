package com.example.leixin.helloworld.broadcast;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.leixin.helloworld.R;

public class DynamicBroadcastActivity extends AppCompatActivity implements View.OnClickListener {

    private IntentFilter intentFilter;
    private NetworkChangeReceiver networkChangeReceiver;
    private AnotherCustomReceiver anotherCustomReceiver;
    private ThirdCustomReceiver thirdCustomReceiver;
    private LocalReceiver localReceiver;

    private LocalBroadcastManager localBroadcastManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_broadcast);

        intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        networkChangeReceiver = new NetworkChangeReceiver();
        registerReceiver(networkChangeReceiver, intentFilter);

        intentFilter = new IntentFilter();
        intentFilter.addAction("com.example.leixin.helloworld.broadcast.CUSTOM_BROADCAST");
        anotherCustomReceiver = new AnotherCustomReceiver();
        registerReceiver(anotherCustomReceiver, intentFilter);

        intentFilter = new IntentFilter();
        intentFilter.addAction("com.example.leixin.helloworld.broadcast.CUSTOM_BROADCAST");
        intentFilter.setPriority(100);
        thirdCustomReceiver = new ThirdCustomReceiver();
        registerReceiver(thirdCustomReceiver, intentFilter);

        Button button_send_custom_broadcast = (Button) findViewById(R.id.button_send_custom_broadcast);
        button_send_custom_broadcast.setOnClickListener(this);

        Button button_send_ordered_broadcast = (Button) findViewById(R.id.button_send_ordered_broadcast);
        button_send_ordered_broadcast.setOnClickListener(this);

        Button button_send_local_broadcast = (Button) findViewById(R.id.button_send_local_broadcast);
        button_send_local_broadcast.setOnClickListener(this);

        Button button_login_sample = (Button) findViewById(R.id.button_login_sample);
        button_login_sample.setOnClickListener(this);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);

        intentFilter = new IntentFilter();
        intentFilter.addAction("com.example.leixin.helloworld.broadcast.LOCAL_BROADCAST");
        localReceiver = new LocalReceiver();
        localBroadcastManager.registerReceiver(localReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkChangeReceiver);
        unregisterReceiver(anotherCustomReceiver);
        unregisterReceiver(thirdCustomReceiver);
        localBroadcastManager.unregisterReceiver(localReceiver);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.button_send_custom_broadcast:
                Intent intent = new Intent("com.example.leixin.helloworld.broadcast.CUSTOM_BROADCAST");
                // android 8.0上必须加一下两句才能唤起静态注册的receiver
                ComponentName componentName = new ComponentName(this, CustomReceiver.class);
                intent.setComponent(componentName);
                sendBroadcast(intent);
                break;
            case R.id.button_send_ordered_broadcast:
                Intent intent2 = new Intent("com.example.leixin.helloworld.broadcast.CUSTOM_BROADCAST");
                sendOrderedBroadcast(intent2, null);
                break;
            case R.id.button_send_local_broadcast:
                Intent intent3 = new Intent("com.example.leixin.helloworld.broadcast.LOCAL_BROADCAST");
                localBroadcastManager.sendBroadcast(intent3);
                break;
            case R.id.button_login_sample:
                Intent intent4 = new Intent(DynamicBroadcastActivity.this, LoginActivity.class);
                startActivity(intent4);
                break;
        }
    }

    private class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            // 指的是数据网络，可以上网看网页的
            if (networkInfo != null && networkInfo.isAvailable()) {
                Toast.makeText(context, "network is available", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "network is unavailable", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class LocalReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, "receive local broadcast", Toast.LENGTH_SHORT).show();
        }
    }
}
