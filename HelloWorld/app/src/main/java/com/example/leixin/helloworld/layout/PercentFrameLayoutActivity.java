package com.example.leixin.helloworld.layout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.leixin.helloworld.R;

public class PercentFrameLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_percent_frame_layout);
    }
}
