package com.example.leixin.helloworld.lifecycle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.leixin.helloworld.R;

public class NormalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.normal_layout);
    }
}
