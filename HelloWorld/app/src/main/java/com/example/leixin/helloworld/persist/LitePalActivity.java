package com.example.leixin.helloworld.persist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;

import org.litepal.crud.DataSupport;
import org.litepal.tablemanager.Connector;

import java.util.List;

public class LitePalActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LitePalActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_litepal);

        Button button_sqlite_create = (Button) findViewById(R.id.button_sqlite_create);
        button_sqlite_create.setOnClickListener(this);
        Button button_sqlite_add = (Button) findViewById(R.id.button_sqlite_add);
        button_sqlite_add.setOnClickListener(this);
        Button button_sqlite_update = (Button) findViewById(R.id.button_sqlite_update);
        button_sqlite_update.setOnClickListener(this);
        Button button_sqlite_update_all = (Button) findViewById(R.id.button_sqlite_update_all);
        button_sqlite_update_all.setOnClickListener(this);
        Button button_sqlite_delete = (Button) findViewById(R.id.button_sqlite_delete);
        button_sqlite_delete.setOnClickListener(this);
        Button button_sqlite_query = (Button) findViewById(R.id.button_sqlite_query);
        button_sqlite_query.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_sqlite_create:
                Connector.getDatabase();
                break;
            case R.id.button_sqlite_add:
                Book book = new Book();
                book.setName("The Da Vinci Code");
                book.setAuthor("Dan Brown");
                book.setPages(454);
                book.setPrice(16.96);
                book.setPress("Unknow");
                book.save();
                break;
            case R.id.button_sqlite_update:
                Book book2 = new Book();
                book2.setName("The Lost Symbol");
                book2.setAuthor("Dan Brown");
                book2.setPages(510);
                book2.setPrice(19.95);
                book2.setPress("Unknow");
                book2.save();
                book2.setPrice(10.99);
                book2.save();
                break;
            case R.id.button_sqlite_update_all:
                Book book3 = new Book();
                book3.setPress("Anchor");
                book3.setPrice(14.95);
                book3.updateAll("name = ? and author = ?", "The Lost Symbol", "Dan Brown");
                break;
            case R.id.button_sqlite_delete:
                DataSupport.deleteAll(Book.class, "price < ?", "15");
                break;
            case R.id.button_sqlite_query:
                List<Book> books = DataSupport.findAll(Book.class);
                for (Book bk : books) {
                    Log.d(TAG, "onClick: book name is " + bk.getName());
                    Log.d(TAG, "onClick: book author is " + bk.getAuthor());
                    Log.d(TAG, "onClick: book pages is " + bk.getPages());
                    Log.d(TAG, "onClick: book price is " + bk.getPrice());
                    Log.d(TAG, "onClick: book press is " + bk.getPress());
                }
                break;
        }
    }
}
