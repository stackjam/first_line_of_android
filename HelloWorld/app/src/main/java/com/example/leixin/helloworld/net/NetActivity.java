package com.example.leixin.helloworld.net;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;

public class NetActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net);

        Button button_webView = (Button) findViewById(R.id.button_webView);
        button_webView.setOnClickListener(this);
        Button button_http = (Button) findViewById(R.id.button_http);
        button_http.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_webView:
                Intent intent = new Intent(NetActivity.this, WebViewActivity.class);
                startActivity(intent);
                break;
            case R.id.button_http:
                Intent intent2 = new Intent(NetActivity.this, HttpActivity.class);
                startActivity(intent2);
                break;
        }
    }
}
