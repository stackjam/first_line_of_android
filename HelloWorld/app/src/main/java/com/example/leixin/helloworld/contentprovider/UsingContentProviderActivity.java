package com.example.leixin.helloworld.contentprovider;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;

public class UsingContentProviderActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "UsingContentProviderAct";

    public static final String AUTHORITY = "com.example.leixin.helloworld.contentprovider";

    private String newId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_using_content_provider);

        Button button_cp_add = (Button) findViewById(R.id.button_cp_add);
        button_cp_add.setOnClickListener(this);
        Button button_cp_query = (Button) findViewById(R.id.button_cp_query);
        button_cp_query.setOnClickListener(this);
        Button button_cp_update = (Button) findViewById(R.id.button_cp_update);
        button_cp_update.setOnClickListener(this);
        Button button_cp_delete = (Button) findViewById(R.id.button_cp_delete);
        button_cp_delete.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_cp_add:
                Uri uri0 = Uri.parse("content://" + AUTHORITY + "/book");
                ContentValues values = new ContentValues();
                values.put("name", "A Clash of Kings");
                values.put("author", "George Martin");
                values.put("pages", 1040);
                values.put("price", 22.85);
                Uri newUri = getContentResolver().insert(uri0, values);
                newId = newUri.getPathSegments().get(1);
                break;
            case R.id.button_cp_query:
                Uri uri = Uri.parse("content://" + AUTHORITY + "/book");
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        String name = cursor.getString(cursor.getColumnIndex("name"));
                        String author = cursor.getString(cursor.getColumnIndex("author"));
                        int pages = cursor.getInt(cursor.getColumnIndex("pages"));
                        double price = cursor.getDouble(cursor.getColumnIndex("price"));
                        Log.d(TAG, "onClick: book name is " + name);
                        Log.d(TAG, "onClick: book author is " + author);
                        Log.d(TAG, "onClick: book pages is " + pages);
                        Log.d(TAG, "onClick: book price is " + price);
                    }
                    cursor.close();
                }

                break;
            case R.id.button_cp_update:
                Uri uri2 = Uri.parse("content://" + AUTHORITY + "/book" + "/" + newId);
                ContentValues values2 = new ContentValues();
                values2.put("name", "A Storm of Swords");
                values2.put("pages", 1216);
                values2.put("price", 24.05);
                getContentResolver().update(uri2, values2, null, null);
                break;
            case R.id.button_cp_delete:
                Uri uri3 = Uri.parse("content://" + AUTHORITY + "/book" + "/" + newId);
                getContentResolver().delete(uri3, null, null);
                break;
            default:
                break;
        }
    }
}
