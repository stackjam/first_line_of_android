package com.example.leixin.helloworld.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;

public class FragmentSamplesActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_samples);

        Button button_fragment_two_fragment = (Button) findViewById(R.id.button_fragment_two_fragment);
        button_fragment_two_fragment.setOnClickListener(this);
        Button button_fragment_intelligent_fragment = (Button) findViewById(R.id.button_fragment_intelligent_fragment);
        button_fragment_intelligent_fragment.setOnClickListener(this);
        Button button_fragment_battle = (Button) findViewById(R.id.button_fragment_battle);
        button_fragment_battle.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_fragment_intelligent_fragment:
                Intent intent5 = new Intent(FragmentSamplesActivity.this, IntelligentFragmentActivity.class);
                startActivity(intent5);
                break;
            case R.id.button_fragment_two_fragment:
                Intent intent6 = new Intent(FragmentSamplesActivity.this, FragmentActivity.class);
                startActivity(intent6);
                break;
            case R.id.button_fragment_battle:
                Intent intent7 = new Intent(FragmentSamplesActivity.this, NewsActivity.class);
                startActivity(intent7);
                break;
            default:
                break;
        }
    }
}
