package com.example.leixin.helloworld.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;

import com.example.leixin.helloworld.R;
import com.example.leixin.helloworld.list.RecyclerviewActivity;
import com.example.leixin.helloworld.media.CameraAlbumActivity;
import com.example.leixin.helloworld.media.MediaPlayerActivity;
import com.example.leixin.helloworld.media.VideoPlayerActivity;

import java.io.File;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Button button_send_notice = (Button) findViewById(R.id.button_send_notice);
        button_send_notice.setOnClickListener(this);
        Button button_send_big_picture_notice = (Button) findViewById(R.id.button_send_big_picture_notice);
        button_send_big_picture_notice.setOnClickListener(this);
        Button button_camera_album = (Button) findViewById(R.id.button_camera_album);
        button_camera_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NotificationActivity.this, CameraAlbumActivity.class);
                startActivity(intent);
            }
        });

        Button button_media_player = (Button) findViewById(R.id.button_media_player);
        button_media_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NotificationActivity.this, MediaPlayerActivity.class);
                startActivity(intent);
            }
        });

        Button button_video_player = (Button) findViewById(R.id.button_video_player);
        button_video_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NotificationActivity.this, VideoPlayerActivity.class);
                startActivity(intent);
            }
        });
    }

    final String LONG_TEXT = "牛油果（学名：Butyrospermum parkii Kotschy），落叶乔木，高10-15米，胸径达1-1.5米；树冠开展，分枝多而密，茎枝粗壮，多节瘤，常有弯曲现象；树皮厚，不规则开裂，具乳汁。叶长圆形，先端圆或钝，基部圆或钝，幼时上面被锈色柔毛，后两面均无毛，中脉在上面呈凹槽，下面浑圆且十分凸起，侧脉30对以上，相互平行，两面稍凸起，网脉细；叶柄圆形。花梗被锈色柔毛；花萼裂片披针形，外面被毛；花有香甜味，花冠裂片卵形，全缘。浆果球形，直径3-4厘米，可食，味如柿子；种子卵圆形，黄褐色，具光泽，疤痕侧生，长圆形。花期6月，果期10月。";

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, RecyclerviewActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        Notification notification = null;


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        switch (view.getId()) {
            case R.id.button_send_notice:
                notification = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setContentTitle("新鲜水果到货")
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(">>>>>又大又甜的水果，快来品尝吧<<<<< " + LONG_TEXT))
                        .setContentText(">>>>>又大又甜的水果，快来品尝吧<<<<< " + LONG_TEXT)
                        .setChannelId(channelId)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setSound(Uri.fromFile(new File("/system/media/audio/ringtones/Dream.ogg")))// 不生效呢
                        .setVibrate(new long[]{0, 1000, 1000, 1000})// 需要AndroidManifest VIBRATE权限,但加了也不生效
                        .setLights(Color.GREEN, 1000, 1000)
                        //.setDefaults(NotificationCompat.DEFAULT_ALL) // 全部使用默认设置
                        .setWhen(System.currentTimeMillis() + 10000).build();

                int notificationId = 1;
                notificationManager.notify(notificationId, notification);
                break;
            case R.id.button_send_big_picture_notice:
                notification = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setContentTitle("新鲜水果到货")
                        .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(BitmapFactory.decodeResource(getResources(), R.drawable.img_1)))
                        .setContentText(">>>>>又大又甜的水果，快来品尝吧<<<<< " + LONG_TEXT)
                        .setChannelId(channelId)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setSound(Uri.fromFile(new File("/system/media/audio/ringtones/Dream.ogg")))// 不生效呢
                        .setVibrate(new long[]{0, 1000, 1000, 1000})// 需要AndroidManifest VIBRATE权限,但加了也不生效
                        .setLights(Color.GREEN, 1000, 1000)
                        //.setDefaults(NotificationCompat.DEFAULT_ALL) // 全部使用默认设置
                        .setWhen(System.currentTimeMillis() + 10000).build();

                int notificationId2 = 2;
                notificationManager.notify(notificationId2, notification);
                break;
            default:
                break;
        }
    }
}
